# Copyright 2021 Ridai Govinda Pombo
# Distributed under the terms of the GNU General Public License v2

SUMMARY="Cryptocurrency exchange platform"
DESCRIPTION="
World's largest crypto exchange
"
HOMEPAGE="https://binance.com"

UPSTREAM_DOCUMENTATION="
    https://www.binance.com/en/community [[ description = [ Binance Telegram multilanguage channels and groups ] ]]    
"

DOWNLOADS="https://ftp.binance.com/electron-desktop/linux/production/binance-amd64-linux.deb -> ${PN}-${PV}-x86_64.deb"

DEPENDENCIES="
    run:
        app-arch/bzip2
        app-arch/xz
        app-crypt/argon2
        dev-libs/at-spi2-core
        dev-libs/expat
        dev-libs/fribidi
        dev-libs/glib:2
        dev-libs/gnutls
        dev-libs/json-c
        dev-libs/libbsd
        dev-libs/libepoxy
        dev-libs/libffi
        dev-libs/libmd
        dev-libs/libtasn1
        dev-libs/libunistring
        dev-libs/nettle
        dev-libs/nspr
        dev-libs/nss
        dev-libs/pcre
        gnome-platform/GConf:2
        media-libs/fontconfig
        media-libs/freetype:2
        net-print/cups
        sys-apps/dbus
        sys-sound/alsa-lib
        x11-libs/cairo
        x11-libs/gdk-pixbuf:2.0
        x11-libs/gtk+:3
        x11-libs/libX11
        x11-libs/libXScrnSaver
        x11-libs/libXcomposite
        x11-libs/libXcursor
        x11-libs/libXdamage
        x11-libs/libXext
        x11-libs/libXfixes
        x11-libs/libXi
        x11-libs/libXrandr
        x11-libs/libXrender
        x11-libs/libXtst
        x11-libs/libxcb
        x11-libs/pango
        x11-libs/pixman
"

WORK=${WORKBASE}

pkg_setup() {
    default
    exdirectory --allow /opt
}

src_unpack() {
    default
    edo tar xf data.tar.*
}

src_install() {

    edo mv opt/Binance opt/${PN}-${PV}

    edo mv opt ${IMAGE}
    edo mv usr ${IMAGE}

    dodir /usr/$(exhost --target)/bin
    dosym /opt/${PN}-${PV}/${PN} /usr/$(exhost --target)/bin/${PN}
    dosym /opt/${PN}-${PV} /opt/Binance
}
